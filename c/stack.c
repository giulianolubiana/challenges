#include "stack.h"

const uint32_t kInitAllocSize = 4; 

void stackNew(stack* s, size_t elemSize) {
  s->elemSize = elemSize;
  s->base = malloc(kInitAllocSize * elemSize);
  s->top = s->base;
  s->elemsNum = 0;
  s->stackSize = kInitAllocSize;
}

void stackDestruct(stack* s) {
  free(s->base);
}

void stackPush(stack* s, const void* elemAddr) {
  if (s->elemsNum == s->stackSize) {
    s->stackSize += kInitAllocSize;
    s->base = realloc(s->base, s->elemSize * s->elemsNum);
  }

  // Now the stack has been expanded if necessary we have to
  // compute the address of the new element

  /*          |----- new element address
              v
    top -> | | |a|b| <- base

    new elem = base + elemsNum * elemSize
   */
  s->top += s->elemSize;
  memcpy(s->top, elemAddr, s->elemSize);
  s->elemsNum++;
}

void stackPop(stack *s, void* elemAddr) {
  if (!stackEmpty(s)) {
    memcpy(elemAddr, s->top, s->elemSize);
    s->top -= s->elemSize;
    s->elemsNum--;
  }
}

bool stackEmpty(const stack *s) {
  return s->elemsNum == 0;
}
