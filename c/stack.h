#ifndef STACK_H
#define STACK_H

#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>

typedef struct {
  void *base, *top;
  size_t elemSize;
  uint32_t elemsNum;
  size_t stackSize;
}stack;

void stackNew(stack *s, size_t elemSize);
void stackDesturct(stack* s);
void stackPush(stack *s, const void* elemAddr);
void stackPop(stack *s, void* elemAddr);
bool stackEmpty(const stack* s);

#endif  // STACK_H
