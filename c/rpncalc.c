// Copyright [2017] <Giuliano Lubiana>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "stack.h"

float operation(float a, float b, char simbol);

int main(int argc, char* argv[argc+1]) {
  if (argc == 1) {
    printf("Error: You have to provide an arugument\n");
    return 1;
  }

  float num;
  stack floatStack;

  stackNew(&floatStack, sizeof(num));

  for (int32_t i = 1; i < argc; i++) {
    char* c = argv[i];
    if (*c == '+' || *c == '-' || *c == '*' || *c == '/') {
      float a, b;
      stackPop(&floatStack, &b);
      stackPop(&floatStack, &a);
      float r = operation(a, b, *c);
      stackPush(&floatStack, &r);
    }
    else {
      float f = atof(c);
      stackPush(&floatStack, &f);
    }
  }
  float res;
  stackPop(&floatStack, &res);
  printf("%f\n", res);
  return 0;
}


float operation(float a, float b, char simbol) {
  switch (simbol) {
  case ('+') :
    return a + b;
  case ('-') :
    return a - b;
  case ('*') :
    return a * b;
  case ('/') :
    return a / b;
  default:
    printf("%c isn't supported operation\n", simbol);
    return 0;
  }
}
